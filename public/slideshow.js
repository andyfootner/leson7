/**
 * Adapted from https://github.com/leemark/better-simple-slideshow
 */
var makeSlideshow = function (el, options) {
  var $slideshows = document.querySelectorAll(el), // select all the slides
    $slideshow = {},
    Slideshow = {
      init: function (el, options) {
        options = options || {}; // if options object not passed in, then set to empty object
        options.auto = options.auto || false; // if options.auto object not passed in, then set to false
        this.opts = {
          selector: (typeof options.selector === "undefined") ? "figure" : options.selector,
          auto: (typeof options.auto === "undefined") ? false : options.auto,
          speed: (typeof options.auto.speed === "undefined") ? 1500 : options.auto.speed,
          pauseOnHover: (typeof options.auto.pauseOnHover === "undefined") ? false : options.auto.pauseOnHover,
        };

        this.counter = 0; // to keep track of current slide
        this.el = el; // current slideshow container
        this.$items = el.querySelectorAll(this.opts.selector);
        this.numItems = this.$items.length; // total number of slides
        this.$items[0].classList.add('slideshow-show'); // add show class to first figure
        this.injectControls(el);
        this.addEventListeners(el);
        if (this.opts.auto) {
          this.autoCycle(this.el, this.opts.speed, this.opts.pauseOnHover);
        }
      },
      showCurrent: function (i) {
        // increment or decrement this.counter depending on whether i === 1 or i === -1
        if (i > 0) {
          this.counter = (this.counter + 1 === this.numItems) ? 0 : this.counter + 1;
        } else {
          this.counter = (this.counter - 1 < 0) ? this.numItems - 1 : this.counter - 1;
        }

        // remove .show from whichever element currently has it 
        // http://stackoverflow.com/a/16053538/2006057
        [].forEach.call(this.$items, function (el) {
          el.classList.remove('slideshow-show');
        });

        // add .show to the one item that's supposed to have it
        this.$items[this.counter].classList.add('slideshow-show');
      },
      // build and inject prev/next controls
      injectControls: function (el) {
        // first create all the new elements
        var spanPrev = document.createElement("span"),
          spanNext = document.createElement("span"),
          docFrag = document.createDocumentFragment();

        // add classes
        spanPrev.classList.add('slideshow-prev');
        spanNext.classList.add('slideshow-next');

        // add contents
        spanPrev.innerHTML = '&laquo;';
        spanNext.innerHTML = '&raquo;';

        // append elements to fragment, then append fragment to DOM
        docFrag.appendChild(spanPrev);
        docFrag.appendChild(spanNext);
        el.appendChild(docFrag);
      },
      addEventListeners: function (el) {
        var that = this;
        el.querySelector('.slideshow-next').addEventListener('click', function () {
          that.showCurrent(1); // increment & show
        }, false);

        el.querySelector('.slideshow-prev').addEventListener('click', function () {
          that.showCurrent(-1); // decrement & show
        }, false);

        el.onkeydown = function (e) {
          e = e || window.event;
          // Arrow left.
          if (e.keyCode === 37) {
            that.showCurrent(-1); // decrement & show
          // Arrow right.
          } else if (e.keyCode === 39) {
            that.showCurrent(1); // increment & show
          }
        };
      },
      autoCycle: function (el, speed, pauseOnHover) {
        var that = this,
          interval = window.setInterval(function () {
            that.showCurrent(1); // increment & show
          }, speed);

        if (pauseOnHover) {
          el.addEventListener('mouseover', function () {
            clearInterval(interval);
            interval = null;
          }, false);
          el.addEventListener('mouseout', function () {
            if(!interval) {
              interval = window.setInterval(function () {
                that.showCurrent(1); // increment & show
              }, speed);
            }
          }, false);
        } // end pauseonhover

      },
    }; // end Slideshow object .....
  // make instances of Slideshow as needed
  [].forEach.call($slideshows, function (el) {
    $slideshow = Object.create(Slideshow);
    $slideshow.init(el, options);
  });
};
