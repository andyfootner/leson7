document.addEventListener('DOMContentLoaded', function () {

  let noJs = document.querySelectorAll('.no-js');
  for (let i = 0, l = noJs.length; i < l; i++) {
    noJs[i].style.display = 'none';
  }

  function perform() {
    let message = "You'll need to allow the page to use your device's microphone. Then press start below. Note that the sound is turned down at the start to protect your ears, be careful.";
    if (window.confirm(message)) {
      navigator.mediaDevices.getUserMedia({ audio: true, video: false })
      .then(function(stream) {
        let audio = document.querySelector('audio');
        audio.srcObject = stream;
        audio.duration = 274;
        audio.volume = 0.3;
        audio.onplay = function(e) {
          setTimeout((stream) => {stream.getTracks().forEach( (track) => {
            track.stop();
          })}, 274000, audio.srcObject);
        }
      })
      .catch(function(err) {
        /* handle the error */
        console.log('err', err);
      });
    }
  }

  let button = document.querySelector('button');
  button.onclick = function(){perform();};
});
