document.addEventListener('DOMContentLoaded', function () {

  function addLangSwitcher() {
    let header = document.body.querySelector('header');
    let switcher = document.createElement('div');
    switcher.classList.add('language-switcher');
    let pref = getLangPreference();
    document.documentElement.lang = pref.lang;
    document.documentElement.classList.add(pref.lang);
    textOptions = {'en': 'En', 'es': 'Es', 'fr': 'Fr'};
    for (let i = 0, l = pref.choices.length; i < l; i++) {
      if (pref.choices[i] != pref.lang) {
        let switchLink = document.createElement('a');
        let linkText = document.createTextNode(textOptions[pref.choices[i]] + ' ');
        switchLink.appendChild(linkText);
        switchLink.href = '?lang=' + pref.choices[i];
        switcher.appendChild(switchLink);
      }
    }
    header.prepend(switcher);
  }

  function getLangPreference() {
    let available = ['en', 'es', 'fr']
    let lang = 'en';
    let browserLang = navigator.language || navigator.userLanguage;
    if (available.includes(browserLang)) {
      lang = browserLang;
    }
    let params = new URLSearchParams(window.location.search);
    let langParam = params.get('lang');
    let urlPreference = 0;
    if (langParam !== null) {
      if (available.includes(langParam)) {
        urlPreference = 1;
        lang = langParam;
      }
    }
    return {
      'lang': lang,
      'browserLang': browserLang,
      'chosen': urlPreference,
      'choices': available
    };
  }

  function translate() {

    let pref = getLangPreference();

    if (pref.lang != null && pref.lang != pref.browserLang) {
      let nav = document.body.querySelectorAll('nav a');
      for (let i = 0, l = nav.length; i < l; i++) {
        nav[i].href += '?lang=' + pref.lang;
      }
      let logo = document.body.querySelector('h1 a');
      logo.href += '?lang=' + pref.lang;


      let standoutLinks = document.body.querySelectorAll('#standout-links a');
      for (let i = 0, l = standoutLinks.length; i < l; i++) {
        let hash = standoutLinks[i].hash;
        let href = standoutLinks[i].href.substring(0, standoutLinks[i].href.indexOf('#'));
        standoutLinks[i].href = href + '?lang=' + pref.lang + hash;
      }
      let artistLinks = document.body.querySelectorAll('#artist-links a');
      for (let i = 0, l = artistLinks.length; i < l; i++) {
        artistLinks[i].href += '?lang=' + pref.lang;
      }
      let workLinks = document.body.querySelectorAll('#work-links a');
      for (let i = 0, l = workLinks.length; i < l; i++) {
        workLinks[i].href += '?lang=' + pref.lang;
      }
      // TODO: Only one per page, simplify this.
      let returnLinks = document.body.querySelectorAll('#return a');
      for (let i = 0, l = returnLinks.length; i < l; i++) {
        returnLinks[i].href += '?lang=' + pref.lang;
      }
      let otherLinks = document.body.querySelectorAll('a.translate');
      for (let i = 0, l = otherLinks.length; i < l; i++) {
        otherLinks[i].href += '?lang=' + pref.lang;
      }
    }
    if (pref.lang != null && pref.lang != 'en') {
      document.body.className = pref.lang;
      let all = document.body.querySelectorAll('*');
      for (let i = 0, l = all.length; i < l; i++) {
        if (all[i].dataset[pref.lang] !== undefined) {
          all[i].textContent = all[i].dataset[pref.lang];
        }
      }
    }

    // Set Mailchimp signup language group by context.
    let signup = document.body.querySelectorAll('#mc_embed_signup .input-group input');
    if (signup.length && pref.lang != null) {
      for (let i = 0, l = signup.length; i < l; i++) {
        if (signup[i].classList.contains(pref.lang)) {
          signup[i].checked = true;
          break;
        }
      }
    }
  }

  addLangSwitcher();
  translate();
});
